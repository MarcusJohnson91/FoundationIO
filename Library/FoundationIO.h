/*!
 @header          FoundationIO.h
 @author          Marcus Johnson
 @copyright       2015+
 @version         0.9.7
 @brief           This header contains all of FoundationIO's public API in one easy to include header.
 */

#pragma once

#ifndef FoundationIO_H
#define FoundationIO_H

#define FoundationIO_Version_Major 0
#define FoundationIO_Version_Minor 9
#define FoundationIO_Version_Patch 7

#include "include/ArrayIO.h"
#include "include/AssertIO.h"
#include "include/AsynchronousIO.h"
#include "include/BufferIO.h"
#include "include/CryptographyIO.h"
#include "include/FileIO.h"
#include "include/GUUID.h"
#include "include/MathIO.h"
#include "include/NetworkIO.h"
#include "include/PlatformIO.h"
#include "include/SortIO.h"
#include "include/TestIO.h"
#include "include/TextIO/CommandLineIO.h"
#include "include/TextIO/FormatIO.h"
#include "include/TextIO/LocalizationIO.h"
#include "include/TextIO/LogIO.h"
#include "include/TextIO/StringIO.h"
#include "include/TextIO/StringSetIO.h"
#include "include/TextIO/TextIOTypes.h"


#endif /* FoundationIO_H */
